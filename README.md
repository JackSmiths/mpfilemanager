# MPFileManager

## Installation 安装
### 1.手动安装:
下载DEMO后,将子文件夹MPFileManagerClasses拖入到项目中, 导入头文件MPFileManager.h开始使用.

## Usage 使用方法
#### 实例化方法
`[MPFileManager shareInstance];`

### 1.异步读取文件

```objc
    [[MPFileManager shareInstance] readFile:@"FullFilePath" complete:^(NSData *data) {
        if (data) {
           
        }
    }];
```
### 2.异步写文件（追加方式）
```objc
    [[MPFileManager shareInstance] writeFile:@"FullFilePath"
                                        data:[NSData new]
                                   writeMode:MPFileManagerWriteModeAdd
                                    complete:^(BOOL result) {
        if (result) {
            // 写入成功
        }
```
### 3.异步写文件（覆盖方式）
```objc
    [[MPFileManager shareInstance] writeFile:@"FullFilePath"
                                        data:[NSData new]
                                   writeMode:MPFileManagerWriteModeCover
                                    complete:^(BOOL result) {
        if (result) {
            // 写入成功
        }
    }];
```

## 联系方式:
* WeChat : wzw351420450
* Email : mopellet@foxmail.com
* Resume : [个人简历](https://github.com/MoPellet/Resume)