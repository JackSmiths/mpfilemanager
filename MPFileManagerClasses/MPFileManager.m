
//
//  Created by mopellet on 17/3/8.
//  Copyright © 2017年 eegsmart. All rights reserved.
//

#import "MPFileManager.h"

//线程队列名称
static char *queueName = "FileManagerQueue";

@interface MPFileManager ()
{
    //读写队列
    dispatch_queue_t _queue;
}

@end

@implementation MPFileManager

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    if(self = [super init]) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)readFile:(NSString *)path
            complete:(void (^)(NSData *data))complete
{
            dispatch_async(_queue, ^{
                NSData *data;
                if([[NSFileManager defaultManager] fileExistsAtPath:path]){
                    data = [[NSFileManager defaultManager] contentsAtPath:path];
                }
                if (complete) {
                    complete(data);
                }
            });
}

- (void)writeFile:(NSString *)path
             data:(NSData *)data
        writeMode:(MPFileManagerWriteMode)writeMode
         complete:(void (^)(BOOL result))complete
{
     __block BOOL result = NO;
     NSFileManager *fileManager = [NSFileManager defaultManager];
    switch (writeMode) {
        case MPFileManagerWriteModeAdd:
        {
            dispatch_barrier_async(_queue, ^{
               
                if(![fileManager fileExistsAtPath:path]) //如果不存在
                {
                    result = [fileManager createFileAtPath:path contents:nil attributes:nil];
                    NSData *createData = [NSData new];
                    [createData writeToFile:path atomically:YES];
                }
                NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:path];
                [fileHandle seekToEndOfFile];  //将节点跳到文件的末尾
                [fileHandle writeData:data]; //追加写入数据
                [fileHandle closeFile];
                
                if (complete) {
                    complete(result);
                }
            });
        }
            break;
        case MPFileManagerWriteModeCover:
        {
            dispatch_barrier_async(_queue, ^{
                if([fileManager fileExistsAtPath:path]){
                    [fileManager removeItemAtPath:path error:nil];
                }
                
                result = [[NSFileManager defaultManager] createFileAtPath:path contents:data attributes:nil];
                
                if (complete) {
                    complete(result);
                }
            });
        }
            break;
    }
}

@end
