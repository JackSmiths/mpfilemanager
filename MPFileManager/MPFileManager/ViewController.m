//
//  ViewController.m
//  MPFileManager
//
//  Created by mopellet on 2017/5/20.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import "ViewController.h"
#import "MPFileManager.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[MPFileManager shareInstance] readFile:@"FullFilePath" complete:^(NSData *data) {
        if (data) {
           
        }
    }];
    
    [[MPFileManager shareInstance] writeFile:@"FullFilePath"
                                        data:[NSData new]
                                   writeMode:MPFileManagerWriteModeAdd
                                    complete:^(BOOL result) {
        if (result) {
            // 写入成功
        }
    }];
    
    [[MPFileManager shareInstance] writeFile:@"FullFilePath"
                                        data:[NSData new]
                                   writeMode:MPFileManagerWriteModeCover
                                    complete:^(BOOL result) {
        if (result) {
            // 写入成功
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
